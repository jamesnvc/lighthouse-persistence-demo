//
//  ViewController.m
//  GroceryTotal
//
//  Created by James Cash on 25-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

static NSString *kTaxRateKey = @"taxRate";
static NSString *itemStoreFile = @"itemAmounts.plist";

@interface ViewController () {
    NSMutableArray *itemAmounts;
    double taxRate;
    NSNumberFormatter *currencyFormatter;
}

@end

@implementation ViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *taxRateNum = [defaults objectForKey:kTaxRateKey];
    if (taxRateNum) {
        taxRate = [taxRateNum doubleValue];
        self.taxRateField.text = [NSString stringWithFormat:@"%.2f", taxRate];
    } else {
        taxRate = [self.taxRateField.text floatValue];
    }
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:itemStoreFile];
    NSFileManager *fm = [[NSFileManager alloc] init];
    if ([fm fileExistsAtPath:path]) {
        itemAmounts =
        [NSMutableArray arrayWithArray:[NSArray arrayWithContentsOfFile:path]] ;
    } else {
        itemAmounts = [[NSMutableArray alloc] init];
    }
    currencyFormatter = [[NSNumberFormatter alloc] init];
    currencyFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - actions

- (void)recalculateTotal
{
    double total = 0;
    for (NSNumber *amount in itemAmounts) {
        total += [amount floatValue] * (1 + taxRate);
    }
    self.totalAmountLabel.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:total]];
}

- (void)saveItems
{
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:itemStoreFile];
    [itemAmounts writeToFile:path atomically:YES];
}

- (IBAction)addAmount:(id)sender {
    NSNumber *newAmount = [NSNumber numberWithFloat:[self.amountField.text floatValue]];
    [itemAmounts addObject:newAmount];
    [self saveItems];
    [self recalculateTotal];
    [self.addedItemsTable reloadData];
    self.amountField.text = @"";
}

- (IBAction)setTaxRate:(id)sender {
    taxRate = [self.taxRateField.text floatValue];
    [[NSUserDefaults standardUserDefaults] setObject:@(taxRate)
                                              forKey:kTaxRateKey];
    [self recalculateTotal];
}

- (IBAction)clearAdded:(id)sender {
    [itemAmounts removeAllObjects];
    [self recalculateTotal];
    [self saveItems];
    [self.addedItemsTable reloadData];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellIdentifier"];
    cell.textLabel.text = [currencyFormatter stringFromNumber:[itemAmounts objectAtIndex:indexPath.item]];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemAmounts count];
}

@end
