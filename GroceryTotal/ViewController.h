//
//  ViewController.h
//  GroceryTotal
//
//  Created by James Cash on 25-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *amountField;
@property (weak, nonatomic) IBOutlet UITextField *taxRateField;
@property (weak, nonatomic) IBOutlet UITableView *addedItemsTable;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;

@end

